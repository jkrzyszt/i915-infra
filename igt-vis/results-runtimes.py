#!/usr/bin/env python3
#
# piglit results.json visualizer over history and hosts
#
# Originally by Tomi Sarvela for Intel OTC lab
#
import argparse

from visutils import *

if __name__ == "__main__":
    ap = argparse.ArgumentParser(
        description="Exports test runtimes from piglit result json"
    )
    ap.add_argument("files", help="Files", metavar="files", nargs="+", type=str)
    ap.add_argument(
        "-t",
        "--total",
        default=False,
        action="store_true",
        help="Print total time, sensible only with single result files",
    )

    args = ap.parse_args()

    try:
        jsons = readfiles(args.files)
    except (KeyboardInterrupt) as e:
        print("ERROR: Keyboard interrupt while reading files")
        exit(1)

    tests = parsetests(jsons)
    builds = set([build for build, _ in sorted(jsons, key=groupbuilds)])
    hosts = set([host for _, host in sorted(jsons, key=groupbuilds)])

    if args.total:
        try:
            total = (
                jsons[(*builds, *hosts)]["time_elapsed"]["end"]
                - jsons[(*builds, *hosts)]["time_elapsed"]["start"]
            )
        except:
            total = None

    for test in tests:
        n = 0
        time = 0.0
        dmin = dmax = None
        # Average over all given hosts and builds for one test
        for host in hosts:
            for build in builds:
                # care only about passed tests
                try:
                    res = jsons[(build, host)]["tests"][test]["result"]
                except:
                    continue
                if res == "none" or res == "notrun":
                    continue
                try:
                    dur = (
                        jsons[(build, host)]["tests"][test]["time"]["end"]
                        - jsons[(build, host)]["tests"][test]["time"]["start"]
                    )
                    time += dur
                    n += 1
                except:
                    continue
                if not dmin or dur < dmin:
                    dmin = dur
                if not dmax or dur > dmax:
                    dmax = dur

        if n > 0:
            if len(hosts) > 1 or len(builds) > 1:
                print(
                    "{dmin:6.2f} {t:6.2f} {dmax:6.2f} {test}".format(
                        dmin=dmin or 0.0, dmax=dmax or 0.0, t=time / n, test=test
                    )
                )
            else:
                print("{t:6.2f} {test} {res}".format(t=time / n, test=test, res=res))

    if args.total and total:
        print("{t:6.2f} TOTAL".format(t=total))
