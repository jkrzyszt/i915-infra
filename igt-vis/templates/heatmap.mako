<% from common import htmlname, ttip, htmlttip %>

<?xml version="1.0" encoding="UTF-8"?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>${title} CI Heatmap</title>
    <link rel="stylesheet" href="/assets/results.css" type="text/css" />
    <link rel="stylesheet" href="/assets/heatmap.css" type="text/css" />
    <link rel="shortcut icon" href="${path}favicon.gif" />
  </head>
  <body>
    <table>
      <tr>
        <th></th>
% for host in hosts:
        <th><span class="v">
          <a href="${path}${host}.html"}">${host}</a>
        </th>
% endfor
      </tr>
% for test in tests:
      <tr id="${test}">
        <td class="h"><a href="${path}${htmlname(test)}">${test}</a></td>
    % for host in hosts:
        <td>
<%
        try:
            e=errors[(host,test)]
            t=totals[(host,test)]
            if e==0: p=0
            else: p = int(99*(e/t))+1 # cap between 1..100 to get some color on low percent error rates
            if p==0:
                context.write('<div class="p{p}">&nbsp;</div>'.format(p=p))
            else:
                context.write('<div class="p{p} tooltip">&nbsp;<span><b>{test}<br/>{host}: {e} errors on {t} runs</b></span></div>'.format(p=p, host=host, test=test, e=e, t=t))
        except: # empty cell
            context.write('<div class="p-1">&nbsp;</div>')
%>

    % endfor
% endfor
    </table>
  <p>
  </body>
</html>
