#!/usr/bin/env python3
#
# heatmap for repeat runs
# assumption that all runs fed in are equal
#
# Originally by Tomi Sarvela for Intel OTC lab
#
import os
import argparse

from mako.lookup import TemplateLookup
from mako import exceptions

from visutils import *
from common import *

if __name__ == "__main__":
    ap = argparse.ArgumentParser(description="Visualize set of piglit json results")
    ap.add_argument("-o", "--output", help="Output HTML file", type=str, default="")
    ap.add_argument(
        "-j", "--json", help="Output JSON file for vis.js", type=str, default=""
    )
    ap.add_argument("-t", "--title", help="Title for HTML", type=str, default="")
    ap.add_argument("-p", "--path", help="HTML links' path", type=str, default="")

    ap.add_argument(
        "--testsort",
        default=False,
        action="store_true",
        help="Sort tests in alphabetical order",
    )
    ap.add_argument(
        "-d",
        "--depth",
        type=int,
        default=0,
        help="Relative dir depth of html files, added to links",
    )
    ap.add_argument(
        "-c",
        "--collate",
        type=str,
        default=None,
        help="Collate these results, example: -c notrun,pass",
    )
    ap.add_argument(
        "--combine", type=str, default=None, help="Combine results for similar hosts"
    )
    ap.add_argument(
        "files",
        metavar="files",
        nargs="+",
        type=str,
        help="Piglit json files to be visualized",
    )

    args = ap.parse_args()

    if args.path:
        path = args.path
    else:
        path = "../" * args.depth

    templatename = "heatmap.mako"

    try:
        jsons = readfiles(
            args.files, combine=args.combine
        )  # arranged by [(build,host)]
    except (KeyboardInterrupt) as e:
        print("ERROR: Keyboard interrupt while reading files")
        exit(1)

    collate = args.collate.split(",") if args.collate else None
    tests = parsetests(jsons, sort=args.testsort, collate=collate)

    hostcols = [host for _, host in sorted(jsons, key=grouphosts)]
    hosts = []
    for host in hostcols:
        if not hosts or host != hosts[-1]:
            hosts.append(host)

    totals = {}  # (host,test) = total
    errors = {}  # (host,test) = errors
    builds = set()
    for build, host in jsons:
        if (build, host) not in jsons or jsons[(build, host)] is None:
            continue
        if "tests" not in jsons[(build, host)]:
            continue
        for testname in jsons[(build, host)]["tests"].keys():
            test = jsons[(build, host)]["tests"][testname]
            if "result" not in test:
                continue
            res = test["result"]
            # don't add neither counter
            if res in ["none", "notrun"]:
                continue
            if (host, testname) not in totals:
                totals[(host, testname)] = 0
                errors[(host, testname)] = 0
            totals[(host, testname)] += 1
            if res in ["pass", "skip"]:
                continue
            errors[(host, testname)] += 1

    rootdir = os.path.dirname(os.path.abspath(__file__))
    mako = TemplateLookup([os.path.join(rootdir, "templates"), rootdir])

    try:
        template = mako.get_template(templatename)
    except:
        print(exceptions.text_error_template().render())
        exit(1)

    if args.output:
        with open(args.output, "w") as f:
            try:
                page = template.render(
                    path=path,
                    tests=tests,
                    hosts=hosts,
                    errors=errors,
                    totals=totals,
                    title=args.title,
                )
                f.write(page)
            except:
                print(exceptions.text_error_template().render())
                exit(1)

    exit(0)
