#!/usr/bin/env python3

import pathlib
import requests
import json
import time
import sys
import os
import io
import re

from zipfile import ZipFile
from itertools import groupby

# this is a bridge that posts gitlab pipeline results to patchwork
# the repositories and patchwork's test name (i.e. result name) are hardcoded
# for now, as we have just one project that utilizes it

# put patchwork username / password in .netrc
# store gitlab's API token in GITLAB_TOKEN environment variable
GITLAB_HEADERS = {"Private-Token": os.environ["GITLAB_TOKEN"]}

# store URL where we can download a file mapping artifact name / git tag to
# to patchwork's series revision in form of 1234v3
# e.g. export ARTIFACT_TO_SER_REV_URL_BASE="http://localhost/deploy/{}/patchwork-series.txt"
# {} will get replaced with the tag
ARTIFACT_TO_SER_REV_URL_BASE = os.environ["ARTIFACT_TO_SER_REV_URL_BASE"]

# to use jus lauch ./patchwork_gitlab_igt.py /path/to/state_file
# bootstrap state_file with the pipline id from which on (exclusive) you want
# to post the results to patchwork

GITLAB_PROJECT_URL = "https://gitlab.freedesktop.org/api/v4/projects/3185/"
PATCHWORK_API_BASE = "https://patchwork.freedesktop.org/api/1.0"
PATCHWORK_TEST_NAME = "GitLab.Pipeline"
SLEEP_TIME = 5 * 60  # seconds

ESCAPE_RE = re.compile(r"\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])")


def find_next(pipeline_list, last_processed):
    if last_processed < pipeline_list[-1]["id"]:
        return pipeline_list[-1]

    idx = [x["id"] for x in pipeline_list].index(last_processed)
    idx -= 1  # we want the one that is newer than last_processed

    if idx >= 0:
        return pipeline_list[idx]
    else:
        return None


def map_status_to_patchwork_result(status):
    if status == "success":
        return "success"
    else:
        return "warning"


def get_series_revision_from_ref(ref):
    artifact_name = ref.split("/")[-1]
    resp = requests.get(ARTIFACT_TO_SER_REV_URL_BASE.format(artifact_name))

    if resp.status_code != 200:
        print(
            "Failed to get patchwork series/revision for {} with {}".format(
                artifact_name, resp.status_code
            )
        )
        return (None, None)

    series, revision = resp.text.strip().split("v")
    return (series, revision)


def post_patchwork_result(result, series, revision, summary, result_url):
    url = "{}/series/{}/revisions/{}/test-results/".format(
        PATCHWORK_API_BASE, series, revision
    )
    data = {
        "state": result,
        "summary": summary,
        "url": result_url,
        "test_name": PATCHWORK_TEST_NAME,
    }
    headers = {"content-type": "application/json"}

    print("posting {} with {}".format(url, str(data)))
    resp = requests.post(url, data=json.dumps(data), headers=headers)

    if resp.status_code not in [200, 201]:
        print("failed to publish results with {}!".format(resp.status_code))


def has_failed(x):
    return x["status"] == "failed"


def strip_escape(txt):
    return ESCAPE_RE.sub("", txt)


def groupsort(iterable, key=None):
    return groupby(sorted(iterable, key=key), key=key)


def get_details(pipeline):
    jobs_url = "{}/pipelines/{}/jobs?per_page=100".format(
        GITLAB_PROJECT_URL, pipeline["id"]
    )
    resp = requests.get(jobs_url, headers=GITLAB_HEADERS)
    resp.raise_for_status()
    details = ""

    jobs = resp.json()

    if len(jobs) == 100:
        print("hit the limit of 100 jobs per page, please implement pagination support")
        sys.exit(1)

    grouped_jobs = groupsort(jobs, key=lambda job: job["name"])
    latest_jobs = [
        sorted(jobs, key=lambda job: job["id"])[-1] for name, jobs in grouped_jobs
    ]

    for job in latest_jobs:
        if job["status"] != "failed":
            continue
        details += "\n{} has {} ({}):\n".format(
            job["name"], job["status"], job["web_url"]
        )
        log_url = "{}/jobs/{}/trace".format(GITLAB_PROJECT_URL, job["id"])
        log_resp = requests.get(log_url, headers=GITLAB_HEADERS)
        log_resp.raise_for_status()
        last_lines = log_resp.content.decode().splitlines()[-20:]
        details += "  " + strip_escape("\n  ".join(last_lines))
        details += "\n"

    return details


def process(pipeline, last_postmerge_ref):
    print("processing {}".format(str(pipeline)))
    result = map_status_to_patchwork_result(pipeline["status"])
    details = ""

    if has_failed(pipeline):
        details = get_details(pipeline)

    series, revision = get_series_revision_from_ref(pipeline["ref"])
    if series and revision:
        url = pipeline["web_url"]

        summary = "Pipeline status: {}.\n\nsee {} for the overview.\n{}".format(
            pipeline["status"].upper(), url, details
        )
        post_patchwork_result(result, series, revision, summary, url)


def is_postmerge(pipeline):
    return pipeline["ref"].startswith("intel/IGT_")


def main():
    state_file = pathlib.Path(sys.argv[1])
    state = json.loads(state_file.read_text())

    while True:
        resp = requests.get(
            "{}/pipelines/".format(GITLAB_PROJECT_URL), headers=GITLAB_HEADERS
        )

        if resp.status_code != 200:
            print("Got {}".format(resp.status_code))
            continue

        pipelines = resp.json()
        nxt = find_next(pipelines, state["last_pipeline_id"])

        if nxt and (nxt["status"] not in ["running", "pending"]):
            state["last_pipeline_id"] = nxt["id"]
            if is_postmerge(nxt):
                state["last_postmerge_ref"] = nxt["ref"]
            else:
                process(nxt, state["last_postmerge_ref"])
            state_file.write_text(json.dumps(state))
        else:
            print("nihil novi")
            time.sleep(SLEEP_TIME)


main()
